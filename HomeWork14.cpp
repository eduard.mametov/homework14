﻿#include <iostream>
#include <string>

int main()
{
    setlocale(LC_ALL, "Russian");
    std::string name("Skillbox");
    
    std::cout << name << "\n";
    std::cout << "Первый символ строки: " << name.front() << "\n";
    std::cout << "Последний символ строки: " << name.back() << "\n";
    std::cout << "Длина строки: " << name.length() << "\n";

    return 0;
}

